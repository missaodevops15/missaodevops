---
title: Hello Docusauro.io
author: Vitor Costa 
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. 

<!--truncate-->

## Agora sim! 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.


```
drwxrwxr-x   7 vitor-costa vitor-costa   4096 mai  5 18:26 ./
drwxrwxr-x   5 vitor-costa vitor-costa   4096 mai  5 18:29 ../
drwxrwxrwx   2 vitor-costa vitor-costa   4096 mai  5 18:26 blog/
drwxrwxr-x   2 vitor-costa vitor-costa   4096 mai  5 18:26 core/
drwxrwxr-x 717 vitor-costa vitor-costa  20480 mai  5 18:26 node_modules/
-rw-rw-r--   1 vitor-costa vitor-costa    376 mai  5 18:26 package.json
-rw-rw-r--   1 vitor-costa vitor-costa 830864 mai  5 18:26 package-lock.json
drwxrwxr-x   3 vitor-costa vitor-costa   4096 mai  5 18:26 pages/
-rw-rw-r--   1 vitor-costa vitor-costa   4072 mai  5 18:26 README.md
-rw-rw-r--   1 vitor-costa vitor-costa    174 mai  5 18:26 sidebars.json
-rw-rw-r--   1 vitor-costa vitor-costa   3209 mai  5 18:26 siteConfig.js
drwxrwxr-x   4 vitor-costa vitor-costa   4096 mai  5 18:26 static/

```
